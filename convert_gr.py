# -*- coding: utf-8 -*-
"""
Created on Sun Apr 08 12:37:21 2018

@author: thens
"""

#from os import sys
#if (sys.argv != 1):
#    print ("Not enought arguments")
#    sys.exit(0)

#file = "./grammars/prob-simple.gr"

file = "./Earley Parser/grammars/simple.gr"

NT = {}
Rules = []
CRules = {}
f = open(file)
for line in f.readlines():
    line = line.rstrip()
    words = line.split("\t")
    NT[words[1]] = 1
    Rules.append({'prob': words[0], 'lhs': words[1], 'rhs': words[2]})

rule_order = []
for rule in Rules:
    rhs = rule['rhs'].split(" ")
    rhs_mod = []
    for r in rhs:
        if (NT.has_key(r)):
            rhs_mod.append(r)
        else:
            rhs_mod.append("'{}'".format(r))
    if (CRules.has_key(rule['lhs'])):
        CRules[rule['lhs']] = CRules[rule['lhs']] + " | " + " ".join(rhs_mod) + " [" + rule['prob'] + "]"
    else:
        rule_order.append(rule['lhs'])
        CRules[rule['lhs']] = " ".join(rhs_mod) + " [" + rule['prob'] + "]"
    #print ('{} -> {}'.format(rule['lhs'], " ".join(rhs_mod)))

for lhs in rule_order:
    print ('{} -> {}'.format(lhs,CRules[lhs]))    