////////////////////////////////////////////////////////////////////////////////
//
//	MP 4 - cs585
//
//	Paul Chase
//
//	This implements the final assignment for cs585.
//
//		The task is to implement a language parser; I have chosen to
//	implement the Earley method with probabalistic likelihoods; basically
//	instead of keeping all productions we keep only the K most likely.  this
//	increases efficiency while hopefully having a minimal impact upon the
//	performance of the algorithm.  For the grammar, I am using several by
//	Jason Eisner.
////////////////////////////////////////////////////////////////////////////////

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

/**
 * This class implements an Earley parser for CFG's
 *
 * @author Paul Chase: chaspau@iit.edu
 * @version .68
 * This code is adapted hastily from a previous assignment; it's one of the
 * first things I wrote in java and as such is less than polished.
 */
public class earleyParser {
    //The grammar!
    static Grammar grammar;
    // Threshold for percentage of unknown words to allow in a sentence.
    static int incorrectWordThresholdPercent = 100;

    /**
     * the main function, it reads in a grammar and a file to tag, or gets
     * sentences from the command line.
     * <p>
     * There are two modes, interactive and batch.
     * Interactive: java earleyParser grammar
     * Batch: java earleyParser grammar sentence_file
     *
     * @param argv the standard command line argument array
     */
    public static void main(String argv[]) throws Exception {
        if ((argv.length != 1) && (argv.length != 2)) {
            System.out.println("USAGE: java earleyParser <grammar>[ <sentences file>]");
        } else {
            //load the grammar
            File gf = new File(argv[0]);
            if (gf.exists() && !gf.isDirectory()) {
                grammar = new Grammar(argv[0]);
                if (!grammar.checkProbabilityConsistency()) {
                    System.out.println("ERROR: Grammar is inconsistent, please check the error message and fix");
                } else {
                    //grammar.print();
                    //process sentences from the command line
                    if (argv.length == 1) {
                        System.out.println("Type sentences below.  Note that there is no tokenization\nyou should input each token separated by spaces.  Input \"quit\" to exit.");
                        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                        String temp;
                        String arr[];
                        while (true) {
                            System.out.print("?");
                            temp = br.readLine();
                            arr = temp.split(" ");
                            Production result;
                            //see if they want to exit
                            if ((arr.length == 1) && (arr[0].compareTo("quit") == 0)) {
                                return;
                            }
                            //parse the array of words
                            if (grammar.canParse(arr, incorrectWordThresholdPercent)) {
                                Vector parseTrees = grammar.parse(arr);

                                if (parseTrees != null && !parseTrees.isEmpty()) {
                                    System.out.println("Parsing was successful.");
                                    System.out.println("\nNumber of Parse trees generated: " + parseTrees.size());
                                    printParseTrees(parseTrees);

                                    Production probableParse = getProbableParseTree(parseTrees);
                                    if (Math.exp(probableParse.probability) != 0) {
                                        System.out.println("\nMost probable parse tree has probability: "
                                                + Math.exp(probableParse.probability));
                                        System.out.println("\nTree is as below\n");
                                        probableParse.recursivePrint();
                                    }

                                } else {
                                    System.out.println("Parsing failed!");
                                }
                            } else {
                                System.out.println("That sentence is not in the grammar.");
                            }
                        }
                    } else {
                        //they entered a file
                        File dataFile = new File(argv[1]);
                        if (!dataFile.exists() || dataFile.isDirectory()) {
                            System.out.println("\nInvalid sentences file " + argv[1]);
                            System.out.println("Please enter a valid file path");
                            return;
                        }
                        BufferedReader br = new BufferedReader(new FileReader(dataFile));
                        String line;
                        String arr[];
                        Production result;
                        while ((line = br.readLine()) != null) {
                            if (line.startsWith("#"))
                                continue;
                            arr = line.split(" ");
                            if (grammar.canParse(arr, incorrectWordThresholdPercent)) {
                                System.out.println("=====================================\n" + line);
                                Vector parseTrees = grammar.parse(arr);

                                if (parseTrees != null && !parseTrees.isEmpty()) {
                                    System.out.println("Parsing was successful.!");
                                    System.out.println("\nNumber of Parse trees generated: " + parseTrees.size());
                                    printParseTrees(parseTrees);
                                    System.out.print("\n");

                                    Production probableParse = getProbableParseTree(parseTrees);
                                    if (Math.exp(probableParse.probability) != 0) {
                                        System.out.println("\nMost probable parse tree has probability: "
                                                + Math.exp(probableParse.probability));
                                        System.out.println("\nTree is as below\n");
                                        probableParse.recursivePrint();
                                    }
                                } else {
                                    System.out.println("no matching productions found");
                                }
                            } else {
                                System.out.println("That sentence is not in the grammar.");
                            }
                        }
                    }
                }
            }
        }
    }

    /* Prints a set of parse trees contained in the |parseTrees| array.
     */
    public static void printParseTrees(Vector parseTrees) {
        for (int i = 0; i < parseTrees.size(); i++) {
            System.out.println("\nParse tree: " + (i + 1) + "\n");
            Production parse = (Production) parseTrees.get(i);
            double prob = Math.exp(parse.probability);
            if (prob != 0) {
                System.out.println("Parse tree probability: " + Math.exp(parse.probability));
            }
            parse.backPtrs.get(0).recursivePrint();
            System.out.println();
        }
    }

    /* Returns the parse tree with the highest probability from the list of parses.
     */
    public static Production getProbableParseTree(Vector parseTrees) {
        Production probableParseRoot = (Production) parseTrees.get(0);
        Production probableParse = probableParseRoot.backPtrs.get(0);
        double maxProbability = probableParse.probability;

        for (int i = 0; i < parseTrees.size(); i++) {
            probableParseRoot = (Production) parseTrees.get(i);
            Production parse = probableParseRoot.backPtrs.get(0);
            if (parse.probability > maxProbability) {
                probableParse = parse;
                maxProbability = parse.probability;
            }
        }
        return probableParse;
    }
}
