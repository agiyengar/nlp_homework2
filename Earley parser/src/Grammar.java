////////////////////////////////////////////////////////////////////////////////
//
//      MP 4 - cs585
//
//      Paul Chase
//
//      This implements a cfg style grammar
////////////////////////////////////////////////////////////////////////////////

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

//this class implements the third assignment for CS585
public class Grammar {
	java.util.Map<String, WordDetails> mapWordDetails;
	java.util.Map<String, String> nonTerminalsSet;
	private Vector productions;

	//this reads the grammar in from a file
	public Grammar(String f) throws Exception {
		productions = new Vector();
		productions.clear();

		mapWordDetails = new java.util.HashMap<String, WordDetails>();
        nonTerminalsSet = new java.util.HashMap<String, String>();
		//load the file
		BufferedReader br = new BufferedReader(new FileReader(f));
		Production p;
		String str = br.readLine();
		String rule[];
		while (str != null) {
			rule = str.split("\\t");
			p = new Production();
			p.probability = Math.log((new Float(rule[0])).floatValue());
			p.left = rule[1];
			p.right = rule[2].split(" ");
			p.dot = 0;
			p.start = 0;
			productions.add(p);
			addNonTerminal(rule[1], rule[2]);
			str = br.readLine();
		}
	}

	// function to compare two double with a tolernace of epsilon
	public static boolean double_equals (final double a, final double b) {
		double EPSILON = 0.00001d;
		if (a==b) return true;
		return Math.abs(a - b) < EPSILON;
	}

	public boolean checkProbabilityConsistency() {
		// get list of non terminals
		//   get list of rules for each non-terminals
		///  check if sum of probability is 1 or Double.NEGATIVE_INFINITY
		double TotalProbability = 0;
		boolean passFlag = true;
		System.out.println("INFO: Checking the consistency of the probability of the rules");
		Iterator<String> nonTerminalIter = nonTerminalsSet.keySet().iterator();
		while (nonTerminalIter.hasNext()) {
		    String nonTerminal = nonTerminalIter.next();
            System.out.println("INFO: Checking rules with lhs=" + nonTerminal);
            double TotalProb = 0;
            Vector prods = getProds(nonTerminal);
            for (int j=0; j < prods.size(); j++) {
                Production r = (Production) prods.get(j);
                System.out.println(r.toString() + String.format("  (%.5f)", Math.exp(r.probability)));
                if (r.probability != Double.NEGATIVE_INFINITY) {
                    TotalProb += Math.exp(r.probability);
                }
            }
            if (! (double_equals(TotalProb, 0) || double_equals(TotalProb,1.0d)) ) {
                System.out.println("ERROR: '" + nonTerminal + "' productions are inconsistent. Sum of probability is : " + String.format("%.5f", TotalProb));
                passFlag = false;
                break;
            }
        }
		return passFlag;
	}

	//checks if we've seen this nonterminal; if we haven't, add it
	private void addNonTerminal(String s, String rhs) {
	    if (nonTerminalsSet.containsKey(s))
	        return;
	    nonTerminalsSet.put(s, rhs);
	}

	// Return true if the |s| is a non terminal
	private final boolean isNonTerminal(String s) {
	    return nonTerminalsSet.containsKey(s);
	}

	//this function predicts possible completions of p, and adds them to v
	private final void predict(Vector v, Production p, int pos) {
		Vector prods = getProds(p.right[p.dot]);
		Production q, r;
//		System.out.println("predict:"+p.toString());
		for (int j = 0; j < prods.size(); j++) {
			r = (Production) prods.get(j);
			q = new Production(r);
			q.dot = 0;
			q.start = pos;
			addProd(v, q);
		}
	}

	//this checks if we can scan s on the current production p
	private final boolean scan(Vector v, Production p, String s) {
		Production q;
//		System.out.println("scan:"+p.toString());
		// Match the production rule RHS with the input string. If that fails check if the production rule
		// RHS is a wildcard. If yes and the string |s| is a wildcard word, i.e it did not match with the
		// terminal symbols in the grammar, we add the wildcard prduction rule.
		if ((p.right[p.dot].compareTo(s) == 0) || p.right[p.dot].compareTo("*") == 0 && isWildcardTerminal(s)) {
			p.setPartofSpeechTag(true);
			//match - add it to the next vector
			q = new Production(p);
			if (isWildcardTerminal(s)) {
				q.setWildcardTerminal(true);
				q.setOriginalWord(s);
			}
			q.dot = q.dot + 1;
			addProd(v, q);
			return true;
		}
		return false;
	}

	//this takes a completed production and tries to attach it back in the
	//cols table, putting any attachments into cur.
	private final void attach(Vector cols, Vector cur, Production p) {
		//if the next thing in one rule is the first thing in this rule,
		//we attach.  otherwise ignore
		Vector col;
		Production q, r;
		String s = p.left;
		boolean match = false;

		col = (Vector) cols.get(p.start);
//		System.out.println("attach:"+p.toString());
		for (int j = 0; j < col.size(); j++) {
			q = (Production) col.get(j);
			if (q.right.length > q.dot)
				if (q.right[q.dot].compareTo(s) == 0) {    //Attach!
					r = new Production(q);
					r.dot = r.dot + 1;
					r.probability += p.probability;
					addProd(cur, r);
					r.addBackpointer(p);
				}
		}
	}

	//this parses the sentence
	public final Vector parse(String sent[]) {
		//this is a vector of vectors, storing the columns of the table
		Vector cols = new Vector();
		cols.clear();
		//this is the current column; a vector of production indices
		Vector cur = new Vector();
		cur.clear();
		//this is the next column; a vector of production indices
		Vector next = new Vector();
		next.clear();
		//add the first symbol
		cur.add((Production) getProds("ROOT").get(0));
		Production p;

		Vector parseTrees = new Vector();

		for (int pos = 0; pos <= sent.length; pos++) {
			System.out.println("Pos = " + pos);
			int i = 0;
			boolean match = false;
			//check through the whole vector, even as it gets bigger
			while (i != cur.size()) {
				p = (Production) cur.get(i);
				if (p.right.length > p.dot) {    //predict and scan
					if (sent.length == pos) {
						match = true;
					} else {
						if (isNonTerminal(p.right[p.dot])) {
							//predict adds productions to cur
							predict(cur, p, pos);
						} else {
							//scan adds productions to next
							System.out.println("scan: " + p.toString() + " (" + pos + ")= " + sent[pos]);
							if (scan(next, p, sent[pos])) {
								System.out.println("  Found: " + sent[pos]);
								match = true;
							}
						}
					}
				} else {    //attach
					attach(cols, cur, p);
					if (sent.length == pos) {
						match = true;
					}
				}
				i++;
				//when using a gargantuan grammar
				//this spits out stuff if it's taking a long time.
				if (i % 100 == 0)
					System.out.print(".");
			}
			System.out.println("Match = " + match);
			cols.add(cur);
			if (!match) {
				printTable(cols, sent);
				System.out.println("Failed on: " + cur);
				return null;
			}
			cur = next;
			next = new Vector();
			next.clear();
			System.out.println();
		}

		//print the Earley table
		//Comment this out once you've got parses printing; it's
		//only here for your evaluation.
		//printTable(cols,sent);

		//Right now we simply check to see if a parse exists;
		//in other words, we see if there's a "ROOT -> x x x ."
		//production in the last column.  If there is, it's returned; otherwise
		//return null.
		//TODO: Return a full parse.

		cur = (Vector) cols.get(cols.size() - 1);
		Production finished = new Production((Production) getProds("ROOT").get(0));
		finished.dot = finished.right.length;
		for (int i = 0; i < cur.size(); i++) {
			p = (Production) cur.get(i);
			if (p.equals(finished)) {
				parseTrees.add(p);
			}
		}
		return parseTrees;
	}

	/**
	 * this prints the table in a human-readable fashion.
	 * format is one column at a time, lists the word in the sentence
	 * and then the productions for that column.
	 *
	 * @param cols The columns of the table
	 * @param sent the sentence
	 */
	private final void printTable(Vector cols, String sent[]) {
		Vector col;
		//print one column at a time
		for (int i = 0; i < cols.size(); i++) {
			col = (Vector) cols.get(i);
			//sort the columns by
			if (i > 0) {
				System.out.println("\nColumn " + i + ": " + sent[i - 1] + "\n------------------------");
			} else {
				System.out.println("\nColumn " + i + ": ROOT\n------------------------");
			}

			for (int j = 0; j < col.size(); j++) {
				System.out.println(((Production) col.get(j)).toString());
			}
		}
	}

	//this adds a production p to the vector v of production indices
	//it also checks for duplicate indices, and skips those
	private final void addProd(Vector v, Production p) {
		//check for duplicates
		for (int i = 0; i < v.size(); i++) {
			Production current = (Production) v.get(i);
			if (current.equals(p)) {
                // We may have identical rules with different back pointers. These rules belong in different
                // parse trees. Don't treat them as equals.
                if (!current.backPtrs.equals(p.backPtrs))
                    continue;
				return;
			}
		}
		v.add(p);
	}

	//This runs through the columns and returns all the fully parsed productions
	//i.e. those with little dots at the very end.
	private final Vector getFinalProds(Vector cols) {
		Vector cur;
		Vector prods = new Vector();
		prods.clear();
		Production p;
		for (int i = 0; i < cols.size(); i++) {
			cur = (Vector) cols.get(i);
			for (int j = 0; j < cur.size(); j++) {
				p = (Production) cur.get(j);
				if (p.right.length == p.dot) {
					if (p.left.compareTo("ROOT") != 0) {
						prods.add(p);
					}
				}
			}
		}
		//convert it to an array for returning
		return prods;
	}

	//this returns true if a string is in the grammar, false otherwise
	//it's not exactly "comprehensive"... mostly it'll just see if all
	//the tokens in the sentence are terminals.
	private final boolean inGrammar(String s) {
		boolean found = false;
		Production p;
		for (int i = 0; i < productions.size(); i++) {
			p = (Production) productions.get(i);
			for (int j = 0; j < p.right.length; j++)
				if (p.right[j].equals(s))    // we need exact match not substring match
					found = true;
			//we can't have a string equal to a non-terminal
			if (p.left.compareTo(s) == 0) {
				System.out.println("String contains a non-terminal - cannot parse");
				return false;
			}
		}
		return found;
	}

	//this returns a vector of productions with a left side matching the
	//argument; happy string comparing.
	private final Vector getProds(String left) {
		//we store it in a vector for safekeeping
		Vector prods = new Vector();
		prods.clear();
		Production p;
		for (int i = 0; i < productions.size(); i++) {
			p = (Production) productions.get(i);
			if (p.left.compareTo(left) == 0)
				prods.add(p);
		}
		//convert it to an array for returning
		return prods;
	}

	//this checks if the given string[] has a parse tree with this grammar
	//
	public final boolean canParse(String sent[], int incorrectWordThresholdPercent) {
		int allowedIncorrectWords = (int) ((incorrectWordThresholdPercent / 100.0) * sent.length);
		int incorrectWords = 0;
		// Check if the symbols are in the grammar. We allow upto incorrectWordThresholdPercent
		// unknown words. These words will get matched up by wildcard production rules if they
		// exist in the grammr.
		for (int i = 0; i < sent.length; i++)
			if (!inGrammar(sent[i])) {
				incorrectWords++;
				if (incorrectWords > allowedIncorrectWords)
					return false;
				mapWordDetails.put(sent[i], new WordDetails(sent[i], true));
			} else {
				mapWordDetails.put(sent[i], new WordDetails(sent[i], false));
			}
		return true;
	}

	//this prints out the grammar
	public void print() {
		System.out.println(this.toString());
	}

	//what does every toString function do?
	public String toString() {
		String ret = "";
		for (int i = 0; i < productions.size(); i++)
			ret = ret + ((Production) productions.get(i)).toString() + "\n";
		return ret;
	}

	public boolean isWildcardTerminal(String word) {
		if (!mapWordDetails.containsKey(word)) {
			return false;
		}

		WordDetails details = mapWordDetails.get(word);
		return details.getIsWildcard();
	}

	// Contains information about a word in the sentence like the actual word
	// whether it is a wildcard, etc. Wildcard words are words which are not specified in the
	// grammar. Thee words are matched up by wildcard production rules in the grammar. If the wildcard
	// rules don't exist the parse will fail.
	public class WordDetails {
		private String word;
		private boolean isWildcard;

		WordDetails(String word, boolean isWildcard) {
			this.word = word;
			this.isWildcard = isWildcard;
		}

		boolean getIsWildcard() {
			return isWildcard;
		}
	}
}
