# -*- coding: utf-8 -*-
"""
Created on Sun Apr 08 12:31:57 2018

@author: thens
"""

import nltk
from nltk import CFG
from nltk import PCFG
from nltk.parse import pchart
from nltk.parse.earleychart import EarleyChartParser

cfg_simple2 = """
ROOT -> S 
S -> NP VP
NP -> NP PP | N 
PP -> P NP 
VP -> V NP  | VP PP
N -> 'astronomers' | 'ears'  | 'stars'  | 'telescopes'
P -> 'with' 
V -> 'saw' 
"""

pcfg_simple2 = """
ROOT -> S [1]
S -> NP VP [1]
NP -> NP PP [0.2] | N [0.8]
PP -> P NP [1]
VP -> V NP [0.2] | VP PP [0.8]
N -> 'astronomers' [0.25] | 'ears' [0.25] | 'stars' [0.25] | 'telescopes' [0.25]
P -> 'with' [1]
V -> 'saw' [1]
"""

cfg_simple = """
ROOT -> s
s -> s cc s | np vp | faux np vp | np fis np
np -> np cc np | fname | fname fname | fpron | nbar | fd nbar
nbar -> fn | fn nbar-mods
nbar-mods -> pp | pp nbar-mods
vp -> vp cc vp | vbar | vbar vbar-mods
vbar -> fv | faux fv | vbar np | vbar np np | vbar np pp
vbar-mods -> pp | pp vbar-mods
pp -> fp np
fis -> 'is'
fname -> 'buckaroo' | 'banzai' | 'john' | 'whorfin' | 'planet' | 'earth' | 'chicago' | 'ten'
fn -> 'jetcar' | 'alien' | 'aliens' | 'watermelon' | 'watermelons'
fpron -> 'i' | 'we' | 'us' | 'you' | 'he' | 'she' | 'it' | 'they'
fd -> 'a' | 'the' | 'this' | 'these'
fv -> 'drive' | 'drives' | 'driven' | 'driving' | 'give' | 'gives' | 'live' | 'lives' | 'steal' | 'steals'
faux -> 'do' | 'did' | 'does' | 'will' | 'was' | 'is'
fp -> 'in' | 'on' | 'to' | 'from'
cc -> 'for' | 'and' | 'nor' | 'but' | 'or' | 'yet' | 'so'
"""

pcfg_simple = """
ROOT -> s [1.0]
s -> s-bar [0.9] | s-bar cc s-bar [0.1]
s-bar -> np vp [0.5] | faux np vp [0.3] | np fis np [0.2]
np -> fname [0.1] | fname fname [0.1] | fpron [0.2] | nbar [0.3] | fd nbar [0.3]
nbar -> fn [0.4] | fn nbar-mods [0.6]
nbar-mods -> pp [0.8] | pp nbar-mods [0.2]
vp -> vbar [0.7] | vbar vbar-mods [0.3]
vbar -> fv [0.15] | faux fv [0.18] | vbar np [0.5] | vbar np np [0.02] | vbar np pp [0.15]
vbar-mods -> pp [0.9] | pp vbar-mods [0.1]
pp -> fp np [1]
fis -> 'is' [1]
fname -> 'buckaroo' [0.1] | 'banzai' [0.1] | 'john' [0.1] | 'whorfin' [0.1] | 'planet' [0.1] | 'earth' [0.1] | 'chicago' [0.2] | 'ten' [0.2]
fn -> 'jetcar' [0.2] | 'alien' [0.2] | 'aliens' [0.2] | 'watermelon' [0.2] | 'watermelons' [0.2]
fpron -> 'i' [0.1] | 'we' [0.1] | 'us' [0.1] | 'you' [0.1] | 'he' [0.2] | 'she' [0.2] | 'it' [0.1] | 'they' [0.1]
fd -> 'a' [0.3] | 'the' [0.4] | 'this' [0.15] | 'these' [0.15]
fv -> 'drive' [0.1] | 'drives' [0.1] | 'driven' [0.1] | 'driving' [0.1] | 'give' [0.1] | 'gives' [0.1] | 'live' [0.1] | 'lives' [0.1] | 'steal' [0.1] | 'steals' [0.1]
faux -> 'do' [0.1] | 'did' [0.1] | 'does' [0.1] | 'will' [0.3] | 'was' [0.2] | 'is' [0.2]
fp -> 'in' [0.25] | 'on' [0.25] | 'to' [0.25] | 'from' [0.25]
cc -> 'for' [0.10] | 'and' [0.25] | 'nor' [0.10] | 'but' [0.10] | 'or' [0.25] | 'yet' [0.10] | 'so' [0.10]
"""

cfg_grammar = CFG.fromstring(cfg_simple)
cfg_grammar2 = CFG.fromstring(cfg_simple2)
pcfg_grammar = PCFG.fromstring(pcfg_simple)
pcfg_grammar2 = PCFG.fromstring(pcfg_simple2)

grammar = cfg_grammar

#grammar = pcfg_grammar

#sentence = 'astronomers saw stars with ears'
sentence = 'she drives a jetcar to planet earth'
#sentence = "she gives and john steals"

tokens = sentence.split()

earley = EarleyChartParser(grammar)
chart = earley.chart_parse(tokens)
parses = list(chart.parses(grammar.start()))

#parser = pchart.InsideChartParser(grammar)
#parses = parser.parse(tokens)

print("\n")
print("Total parse trees: ", len(parses))
print("\n")

for tree in parses:
    print(tree)